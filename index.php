/*
Plugin Name: Ctnels UserAgent Check
Plugin URI:  http://ctnels.com 
Description: This plugin checks for useragent and redirects on failure to validate.
Version:     1.0
Author:      Ctnels 
Author URI:  http://ctnels.com 
License:     GPL2 etc
License URI: https://ctnels.com
*/

<?php 
    $UserBrowserAgent = $_SERVER['HTTP_USER_AGENT'];
    $ApprovedBrowserAgent = "IbomApps-Mobile-By-Ctnels";
    
    if ( $UserBrowserAgent != $ApprovedBrowserAgent ) {
        header('Location: https://ibomapps.com.ng/ibomapps/');
        exit(); 
    } 
?>